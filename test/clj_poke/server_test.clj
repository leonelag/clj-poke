(ns clj-poke.server-test
  (:require [clojure.test      :refer :all]
            [ring.mock.request :as mock]
            [clj-poke.server   :refer :all]))

(deftest test-app
  (testing "main route should redirect to /index.html"
    (let [response (app-routes (mock/request :get "/"))]
      (is (= (:status response) 302))))

  (testing "Index and author pages should exist."
    (let [page-index  (app-routes (mock/request :get "/index.html"))
          page-author (app-routes (mock/request :get "/author.html"))]
      (is (= (:status page-index) 200))
      (is (= (:status page-author) 200))))

  (testing "not-found route"
    (let [response (app-routes (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
