(defproject clj-poke "0.1.0-SNAPSHOT"
  :description "clj-poke: JSON API for searching for Pokémon."
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [compojure "1.4.0"]

                 [http-kit "2.1.18"]
                 [org.clojure/data.json "0.2.6"]]
  :plugins [[lein-ring "0.9.7"]]
  :main clj-poke.server
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]]}
   :test {:dependencies [[ring/ring-mock "0.3.0"]]}})
