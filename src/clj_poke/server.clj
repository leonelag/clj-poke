(ns clj-poke.server
  (:require [compojure.core     :refer :all]
            [compojure.route    :as route]
            [org.httpkit.server :as httpkit]
            [clj-poke.client    :as poke]
            [clojure.data.json  :as json]))

(defn redirect [location]
  "Template response for HTTP 302 Redirect."
  {:status   302
   :headers { "Location" location }
   :body     ""})


(defn json-response [status-code body]
  "Template response with the content type and encoding of a response
   in JSON format.

   Body should be in jsonapi format; this function assocs the
   key :status into the resulting JSON response so you don't have to."
  {:status  status-code
   :headers {"Content-Type"  "application/vnd.api+json; charset=UTF-8"}
   :body    (json/write-str body)})

(defn json-error []
  "Template for an error response, as per http://jsonapi.org/format/#errors"
  (json-response 500
                 {:title  "Internal error."
                  :detail "An internal error has occurred."}))


(defn json-not-found [id]
  "Template for a Pokemon not found in the database."
  (json-response 404
                 {:title  "Not found."
                  :detail (str "Pokemon was not found in database: "
                               id) }))


(defn handle-find [id]
  (try
    (if-let [pokemon (poke/pokemon id)]
      (json-response 200 {:data pokemon})
      (json-response 404 {:errors [(json-not-found id)]}))
    (catch java.lang.Exception e
      (json-response 500 {:errors [(json-error)]} ))))


(defroutes app-routes
  (GET "/" [] (redirect "/index.html"))
  (GET "/find/:id" [id] (handle-find id))
  (route/resources "/")
  (route/not-found "Not Found"))


;;
;; From: http://www.http-kit.org/server.html#stop-server
;;
(defonce server (atom nil))

(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! server nil)))

(defn start-server [port ip]
  (reset! server (httpkit/run-server #'app-routes {:port port :ip ip})))


(defn -main []
  (let [port (Integer/parseInt (get (System/getenv) "OPENSHIFT_CLOJURE_HTTP_PORT" "8080"))
        ip (get (System/getenv) "OPENSHIFT_CLOJURE_HTTP_IP" "0.0.0.0")]
    (start-server port ip)))
