(ns clj-poke.client-test
  (:require [clojure.test :refer :all]
            [clj-poke.client :refer :all]))

(deftest pokemon-test
  (testing "Pikachu should be found in the backend, with all attributes"
    (let [pikachu (pokemon "pikachu")]
      (is (= "Pikachu" (:name pikachu)))
      (is (not (nil? (:img pikachu))))
      (is (= 55 (:attack pikachu)))
      (is (= 40 (:defense pikachu)))
      (is (.contains (:description pikachu)
                     "It raises its tail to check")))))

(deftest fetch-pokemon-test
  (testing "Should be able to fetch Pikachu, the Pokemon."
    (pokemon "pikachu")))


(deftest fetch-non-existing
  (testing "Should not find a non-existing Pokemon."
    (is (nil? (pokemon "does-not-exist")))))
