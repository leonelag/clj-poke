# clj-poke

A web API for searching information about Pokémon.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

This application is intended to run with Leiningen, as such:

    lein trampoline run

