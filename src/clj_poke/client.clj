(ns clj-poke.client
  "Pokemon API client.
    A client to the Pokemon API at http://pokeapi.

    Offers a single method `pokemon`, which fetches the pokemon by
    its id."
  (:gen-class)
  (:require [org.httpkit.client :as httpkit]
            [clojure.data.json  :as json]
            [clojure.string     :as string]))


(def url-base
  "Base URL of the backend Pokémon API."
  "http://pokeapi.co")
(def url-pokemon
  "URL for fetching Pokemon"
  (str url-base "/api/v1/pokemon"))

(defn- fetch-json [url]
  "Utility function to fetch and parse JSON from a URL.

   Attempts an HTTP GET. In case of a successful response (status code
   200 OK), reads the body of the response as JSON.

   Returns nil for other status codes."
  (let [response (-> url
                     (httpkit/get)
                     (deref))]
    (case (:status response)
      200 (-> response
              (:body)
              (json/read-str))

      ;; other status codes.
      nil)))

(defn- fetch-pokemon [id]
  "Fetches a pokemon from the backend API by its id."
  (let [url (str url-pokemon "/" id)]
    (fetch-json url)))

(defn- urls-descriptions [pokemon]
  "A seq with all the URLs to descriptions of a Pokemon."
  (let [descriptions (-> pokemon
                         (get "descriptions"))]
    (for [d descriptions]
      (str url-base (get d "resource_uri")))))

(defn- fetch-descriptions [pokemon]
  "Fetches the descriptions of a pokemon.

   Some pokemons have many different descriptions. The descriptions
   are fetched from the API and concatenated.

   Expects input as returned by `fetch-pokemon`."
  (let [urls (urls-descriptions pokemon)
        descs (pmap (fn [url]
                      (-> (fetch-json url)
                          (get "description")))
                    urls)]
    (string/join "\n" descs)))

(defn- url-image [pokemon]
  "Finds the URL to an image of the pokemon.

   This is done in two steps. First, the sprites attribute of the
   pokemon is checked to find the URLs of sprites.

   Then, if there is a sprite URL, it is fetched and the URL of the
   actual image is found in the contents."

  (when-let [sprites (get pokemon "sprites")]
    (when-not (empty? sprites)
      (let [uri (-> sprites
                    (first)
                    (get "resource_uri"))
            ;; The URL of the sprite description
            url-sprite (str url-base uri)

            ;; Fetch sprite description to find URI of image
            uri-image (-> (fetch-json url-sprite)
                          (get "image"))]
        (str url-base uri-image)))))


(defn pokemon [id]
  "Fetches a pokemon from the backend API by its id.
   Returns a promise for the fetched pokemon.
   keys: :name, :img, :attack, :defense, :description"
  (when-let [p (fetch-pokemon id)]
    (assoc {}
           :name    (get p "name")
           :attack  (get p "attack")
           :defense (get p "defense")
           :img     (url-image p)
           :description (fetch-descriptions p))))
